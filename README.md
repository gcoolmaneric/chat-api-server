.Folder  Structure

chat_api_server : the source codes of Chat API Server  

client  : Sample codes to test Chat API in WebSocket

testScript : Stress test script with Artillery


-------------------------------------------------------------------------
How to install client to test Chat API over WebSocket ?
cp client to /var/www/html  

edit server IP at client/testPage.html

open page http://yourServerIP/client/testPage.html


How to do the stress test ? 
1. Install Artillery 

2. artillery run client/testAPIScript
   artillery run client/testSocketIOScript.txt

-------------------------------------------------------------------------
