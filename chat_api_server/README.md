Chat API Server is developed by Spring-boot and Netty socket.io using 
H2 in-memory database to save data.

Import project in Eclipse/Spring STS.

Run project as SpringBootApplication or deploy it on Tomcat.

// Clean  
mvn clean

// Build and Run
mvn spring-boot:run



