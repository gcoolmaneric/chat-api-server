package com.ea.apiserver.model;

public class ChatObject {

    private String userName;
    private String message;
    private String roomId;
    private int cmd;		// 1: join a room, 2: leave a room, 3: send a message
    
    public ChatObject() {
    }

    public ChatObject(String userName, String message) {
        super();
        this.userName = userName;
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getRoomId() {
        return roomId;
    }
    public void setRoomId(String val) {
        this.roomId = val;
    }
    
    public int getCmd() {
        return cmd;
    }
    public void setCmd(int val) {
        this.cmd = val;
    }
}
