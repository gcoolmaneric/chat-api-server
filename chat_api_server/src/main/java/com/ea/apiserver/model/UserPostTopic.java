package com.ea.apiserver.model;


// User Input Class
public class UserPostTopic{

    private String userId;
    private String title;
    private String msg;

    public UserPostTopic() {}

    public void setUserId(String userId) { this.userId = userId;  }
    public String getUserId() { return userId; }

    public void setTitle(String title) { this.title = title;  }
    public String geTitle() { return title; }

    public void setMsg(String msg) { this.msg = msg;  }
    public String getMsg() { return msg; }
}
