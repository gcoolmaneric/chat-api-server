package com.ea.apiserver.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Topic {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String title;
	
	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updatetime;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public Date getUpdateTime() {
		return updatetime;
	}

	public void setUpdateTime(Date val) {
		this.updatetime = val;
	}

	@Override
	public String toString() {
		return "Topic [id=" + id + ", title=" + title + ", updatetime=" + updatetime + "]";
	}
	
}
