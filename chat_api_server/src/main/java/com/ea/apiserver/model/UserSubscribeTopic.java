package com.ea.apiserver.model;


// User Input Class
public class UserSubscribeTopic{

    private String userId;
    private String title;

    public UserSubscribeTopic() {}

    public void setUserId(String userId) { this.userId = userId;  }
    public String getUserId() { return userId; }

    public void setTitle(String title) { this.title = title;  }
    public String geTitle() { return title; }
}
