package com.ea.apiserver.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String userId;
	
	// User`s highest scores 
	@Column(nullable = false)
	private long score;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userid) {
		this.userId = userid;
	}

	public long getScore() {
		return score;
	}

	public void setScore(long val) {
		this.score = val;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userId=" + userId + ", score=" + score + "]";
	}
	
}
