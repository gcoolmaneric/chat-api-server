package com.ea.apiserver.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;

@Entity
public class Notification {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String userId;
	
	@Column(nullable = false)
	private long topicId;
	
	@Column(nullable = false)
	private int status;		// 1: enable, 0: disable
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userid) {
		this.userId = userid;
	}

	public long getTopicId() {
		return topicId;
	}

	public void setTopicId(long val) {
		this.topicId = val;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int val) {
		this.status = val;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", userId=" + userId + ", topicId=" + topicId + "]";
	}
	
}
