package com.ea.apiserver.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ea.apiserver.model.User;
import com.ea.apiserver.service.UserService;

import com.ea.apiserver.common.JsonMsg;


@RestController
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;

	// Login 
	@RequestMapping(value = "/login", method=RequestMethod.GET)
	@ResponseBody
	public JsonMsg login(String uid)
	{
		
		if(uid.length() == 0 ) return new JsonMsg(400, "userId is null");
		if(userService.hasSpecialString(uid)) return new JsonMsg(401, "userId contains special characters ");
			
	    try 
	    {
	    	User user = userService.getUserByUserId(uid);
	    	if(user != null)
	    	{
	    		return new JsonMsg(200, "OK", "User Exists UserId", user.getUserId());
	    	} 
	    	else 
	    	{    
	    		    		
		    	 User newuser = new User();
		    	 newuser.setScore(0);	    	 
		    	 newuser.setUserId(uid);
		    	 userService.addUser(newuser);
		    	 
		    	 return new JsonMsg(200, "Create new User OK", newuser.getUserId());
		     }
	    }
		catch (Exception ex) 
	    {
		    return new JsonMsg(500, "api server error" + ex.toString());
		}
	}
	
	// List Top10 User by scores 
	@RequestMapping(value = "/list/top10", method = RequestMethod.GET)
	public List<User> getUsersByScore() {
		
		return userService.getUsersByScore();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<User> addAllUsers() {
		return userService.getAllUsers();
	}

}
