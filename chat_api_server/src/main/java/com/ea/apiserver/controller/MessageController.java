package com.ea.apiserver.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ea.apiserver.model.Topic;
import com.ea.apiserver.model.UserPostTopic;
import com.ea.apiserver.service.TopicService;
import com.ea.apiserver.ChatEventHandler;
import com.ea.apiserver.common.JsonMsg;
import com.ea.apiserver.model.Message;
import com.ea.apiserver.service.MessageService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("msg")
public class MessageController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ChatEventHandler.class);
	
	@Autowired
	private TopicService topicService;
	@Autowired
	private MessageService msgService;
	

	@RequestMapping(value = "/list/top10", method=RequestMethod.POST, headers = {"Content-type=application/json"})
	@ResponseBody
	public List<Message> top10(@RequestBody UserPostTopic data)
	{	
		 LOGGER.debug(String.format("###  MessageController title:%s",  data.geTitle()));
		 
		if(data.geTitle().length() == 0 ) return null;		
		Topic oldtopic = topicService.getTopicByTitle(data.geTitle());
		return msgService.getAllMessageByTopicId(oldtopic.getId());
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Message> getAllMessages() 
	{
		return msgService.getAllMessages();
	}

	
}
