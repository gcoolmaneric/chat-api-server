package com.ea.apiserver.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ea.chat.score.ScorerService;

import com.ea.apiserver.model.Topic;
import com.ea.apiserver.service.TopicService;
import com.ea.apiserver.model.UserPostTopic;
import com.ea.apiserver.model.UserSubscribeTopic;
import com.ea.apiserver.model.Message;
import com.ea.apiserver.service.MessageService;
import com.ea.apiserver.model.Notification;
import com.ea.apiserver.service.NotificationService;

import com.ea.apiserver.common.JsonMsg;
import com.ea.apiserver.common.Utility;
import com.ea.apiserver.model.User;
import com.ea.apiserver.service.UserService;

@RestController
@RequestMapping("topic")
public class TopicController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TopicService topicService;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageService msgService;
	@Autowired
	private NotificationService notifyService;
	

	@RequestMapping(value = "/listNotify", method = RequestMethod.GET)
	public List<Notification> getAllNotifications() 
	{
		return notifyService.getAllNotifications();
	}
	
	@RequestMapping(value = "/unsubscribe", method=RequestMethod.POST, headers = {"Content-type=application/json"})
	public JsonMsg unsubscribe(@RequestBody UserSubscribeTopic data)
	{
		log.info("Get unsubscribe UserId:" + data.getUserId() + " title:" + data.geTitle());
		
		// Check input parameters
		if(data.getUserId().length() == 0 ) return new JsonMsg(401, "userId is null");
		if(data.geTitle().length() == 0 ) return new JsonMsg(402, "title is null");
		
		Topic oldtopic = null;
		try
		{
			oldtopic = topicService.getTopicByTitle(data.geTitle());	
	    	if(oldtopic == null)
		    {
	    		return new JsonMsg(405, "Unsubscribe Topic Error Topic does not exist", data.getUserId());
		    }
	    	else
	    	{	
	    		String status = "";
	    		Notification oldNotify = notifyService.getNotificationByUserIdAndTopicId(data.getUserId(), oldtopic.getId());
	    		if(oldNotify != null) {
		    		Notification addNotify = new Notification();
	    			oldNotify.setStatus(0);
	    			notifyService.updateNotification(oldNotify);
	    			status = "Unsubscribe Topic OK update notification";
	    		}
	    		return new JsonMsg(200, status, data.getUserId());
	    	}
		}	
		catch (Exception ex) 
	    {
		    return new JsonMsg(500, "api server error" + ex.toString());
		}
	}
	
	@RequestMapping(value = "/subscribe", method=RequestMethod.POST, headers = {"Content-type=application/json"})
	public JsonMsg subscribe(@RequestBody UserSubscribeTopic data)
	{
		log.info("Get subscribe UserId:" + data.getUserId() + " title:" + data.geTitle());
		
		// Check input parameters
		if(data.getUserId().length() == 0 ) return new JsonMsg(401, "userId is null");
		if(data.geTitle().length() == 0 ) return new JsonMsg(402, "title is null");
		
		Topic oldtopic = null;
		try
		{
			oldtopic = topicService.getTopicByTitle(data.geTitle());	
	    	if(oldtopic == null)
		    {
	    		return new JsonMsg(405, "Subscribe Topic Error Topic does not exist", data.getUserId());
		    }
	    	else
	    	{	
	    		String status = "";
	    		Notification oldNotify = notifyService.getNotificationByUserIdAndTopicId(data.getUserId(), oldtopic.getId());
	    		if(oldNotify == null) {
		    		Notification addNotify = new Notification();
		    		addNotify.setUserId(data.getUserId());
		    		addNotify.setTopicId(oldtopic.getId());
		    		addNotify.setStatus(1);
		    		notifyService.addNotification(addNotify);
		    		status = "Subscribe Topic OK create a new notification";
	    		}else {
	    			oldNotify.setStatus(1);
	    			notifyService.updateNotification(oldNotify);
	    			status = "Subscribe Topic OK update notification";
	    		}
	    		return new JsonMsg(200, status, data.getUserId());
	    	}
		}	
		catch (Exception ex) 
	    {
		    return new JsonMsg(500, "api server error" + ex.toString());
		}
	}
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Topic> getAllTopics() 
	{
		return topicService.getAllTopics();
	}

	
	@RequestMapping(value = "/add", method=RequestMethod.POST, headers = {"Content-type=application/json"})
	@ResponseBody
	public JsonMsg add(@RequestBody UserPostTopic data)
	{
		log.info("Get postTopic UserId:" + data.getUserId() + " title:" + data.geTitle() + " msg:" + data.getMsg());
		
		// Check input parameters
		if(data.getUserId().length() == 0 ) return new JsonMsg(401, "userId is null");
		if(data.geTitle().length() == 0 ) return new JsonMsg(402, "title is null");
		if(data.getMsg().length() == 0 ) return new JsonMsg(403, "msg is null");
		
		Topic oldtopic = null;
		try 
	    {
			// Get message`s new scores by EA`s scorer
			ScorerService eaScorer = new ScorerService();
			int newscores = eaScorer.getScorer().score(data.getMsg());
			log.info("Get postTopic UserId:" + data.getUserId() + " newscores:" + newscores + " msg:" + data.getMsg());
			
			// If new scores > old scores and user exists
			User oldUser = userService.getUserByUserId(data.getUserId());
			if(oldUser != null) 
			{
				// Update user data
				int oldscore = (int)oldUser.getScore();
				if(newscores > oldscore) { 
					oldUser.setScore(newscores);
					userService.updateUser(oldUser);
					log.info("Get postTopic Update Score OK UserId:" + data.getUserId() + " newscores:" + newscores + " oldscore:" + oldUser.getScore());
				}
				
			}
			
			// Create a new msg
			Message msg = new Message();
			msg.setContent(data.getMsg());
			msg.setScore(newscores);
			msg.setUserId(data.getUserId());
			msg.setUpdateTime(Utility.getInstance().getCurrentTime());
			
			// Get a topic by title 
	    	oldtopic = topicService.getTopicByTitle(data.geTitle());	
	    	if(oldtopic == null)
		    {
	    		// Create New Topic
	    		Topic newtopic = new Topic();
	    		newtopic.setTitle(data.geTitle());
	    		newtopic.setUpdateTime(Utility.getInstance().getCurrentTime());
	    		Topic updatedtopic = topicService.addTopic(newtopic);
	    		
	    		// Create a new msg
				msg.setTopicId(updatedtopic.getId());
				msgService.addMessage(msg);
				
	    		return new JsonMsg(200, "Create New Topic OK", data.getUserId());
		    } 
		    else 
		    {     
		    	// Create a new msg with old topic`s Id
				msg.setTopicId(oldtopic.getId());
				msgService.addMessage(msg);
				
				// Get a list of UserId from table Notification
	    		List<Notification> notifyList = notifyService.getAllNotificationByTopicId(oldtopic.getId());
	    		if(notifyList.size() > 0 )
	    			 return new JsonMsg(300, "This topic has been subscribed by someone. Update Topic OK", data.getUserId());
	    		else
	    			return new JsonMsg(200, "Update Topic OK", data.getUserId());
			}
		}
		catch (Exception ex) 
	    {
		    return new JsonMsg(500, "api server error" + ex.toString());
		}
	}

	
}
