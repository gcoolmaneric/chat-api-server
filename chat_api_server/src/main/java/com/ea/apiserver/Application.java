package com.ea.apiserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.SpringAnnotationScanner;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	@Bean
	public SocketIOServer socketIOServer() {
	   Configuration config = new Configuration();
	    //config.setHostname(host); // Default Listen 0.0.0.0
	    config.setPort(9099);
	    config.setBossThreads(2);
	    config.setWorkerThreads(4);
	    return new SocketIOServer(config);
	}

	@Bean
	public SpringAnnotationScanner springAnnotationScanner(SocketIOServer ssrv) {
	    return new SpringAnnotationScanner(ssrv);
	}
	    
	public static void main(String[] args) {
        
		 SpringApplication.run(Application.class, args);
    	
    }
}
