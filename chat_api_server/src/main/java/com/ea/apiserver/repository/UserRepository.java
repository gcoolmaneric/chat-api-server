package com.ea.apiserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ea.apiserver.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	@Query("FROM User")
	public List<User> getAllUsers();
	
	public User findByUserId(String userid);
	
	@Query(value="SELECT * FROM USER ORDER BY SCORE DESC LIMIT 10", nativeQuery = true)
	public List<User> getAllUsersByScore(); 
	
}
