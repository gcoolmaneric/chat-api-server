package com.ea.apiserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ea.apiserver.model.Topic;

public interface TopicRepository extends JpaRepository<Topic, Long> {
	@Query("FROM Topic")
	public List<Topic> getAllTopics();

	public Topic findByTitle(String title);
	
	@Query(value="SELECT * FROM TOPIC WHERE TITLE LIKE ?1% ORDER BY UPDATETIME DESC LIMIT 10", nativeQuery = true)
	public List<Topic> getAllTopicByTitle(String title); 
	
	@Query(value="SELECT * FROM TOPIC WHERE TITLE LIKE ?1% ORDER BY UPDATETIME DESC LIMIT 1", nativeQuery = true)
	public Topic getTopicByTitle(String title); 
	
	// Get Topic By Id
	public Topic findById(Long id);
		
}
