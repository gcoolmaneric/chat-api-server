package com.ea.apiserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ea.apiserver.model.Notification;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
	@Query("FROM Notification")
	
	public List<Notification> getAllNotifications();
	
	@Query(value="SELECT * FROM NOTIFICATION WHERE USER_ID LIKE ?1% LIMIT 10", nativeQuery = true)
	public List<Notification> getAllNotificationByUserId(String userId); 
	
	@Query(value="SELECT * FROM NOTIFICATION WHERE TOPIC_ID = ?1 AND STATUS = 1 ORDER BY ID DESC LIMIT 10", nativeQuery = true)
	public List<Notification> getAllNotificationByTopicId(long topicId); 
	
	public Notification findByUserIdAndTopicId(String userId, long topicId); 
	
	public Notification findByUserId(String userid);
	
	
}
