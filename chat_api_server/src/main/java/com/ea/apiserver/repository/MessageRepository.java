package com.ea.apiserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ea.apiserver.model.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {
	
	@Query("FROM Message")
	public List<Message> getAllMessages();
	
	@Query(value="SELECT * FROM MESSAGE WHERE TOPIC_ID = ?1 ORDER BY UPDATETIME DESC LIMIT 10", nativeQuery = true)
	public List<Message> getAllMessageByTopicId(long topicId); 
	
	// Get Msg By Id
	public Message findById(Long id);
		
}
