package com.ea.apiserver.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ea.apiserver.model.Message;
import com.ea.apiserver.repository.MessageRepository;
import com.ea.apiserver.service.MessageService;

import org.springframework.util.Assert;


@Service
public class MessageServiceImpl implements MessageService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MessageRepository messageRepository;

	@Override
	public Message addMessage(Message msg) {
		Assert.notNull(msg, "Message must not be null");
		Message newMsg = messageRepository.save(msg);
		log.debug("Message save successfully ");
		return newMsg;
	}

	@Override
	public Message updateMessage(Message msg) {
		Assert.notNull(msg, "Message must not be null");
		Message oldMsg = messageRepository.findOne(msg.getId());
		Assert.notNull(oldMsg, "Message User must not be null");
		log.debug("{} updated ",oldMsg);
		return messageRepository.saveAndFlush(oldMsg);
	}

	@Override
	public void deleteMessage(long id) {
		Assert.notNull(id, "id must not be null");
		messageRepository.delete(id);
		log.debug("Message with id {} deleted successfully", id);
	}

	@Override
	public Message getMessageById(long id) 
	{
		Assert.notNull(id, "message id must not be null");
		return messageRepository.findById(id);
	}
	
	@Override
	public List<Message> getAllMessages() {
		Assert.notNull(messageRepository.getAllMessages(), "message list must not be null");
		return messageRepository.getAllMessages();
	}
	
	@Override
	public List<Message> getAllMessageByTopicId(long topicId) {
		return messageRepository.getAllMessageByTopicId(topicId);

	}

	
}
