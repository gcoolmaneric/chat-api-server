package com.ea.apiserver.service;

import java.util.List;

import com.ea.apiserver.model.Message;

public interface MessageService {
	
	// Add Message 
	public Message addMessage(Message msg);
	
	// Update Message 
	public Message updateMessage(Message msg);

	// Delete Message 
	public void deleteMessage(long id);

	// Get all Message 
	public List<Message> getAllMessages();
	
	// Get Message By Id
	public Message getMessageById(long id);
	
	// Get Top 10 Messages By Title
	public List<Message> getAllMessageByTopicId(long topicId);
	
}
