package com.ea.apiserver.service;

import java.util.List;

import com.ea.apiserver.model.Notification;

public interface NotificationService {
	
	// Add Notification 
	public void addNotification(Notification data);
	
	// Update Notification 
	public Notification updateNotification(Notification data);

	// Delete Notification 
	public void deleteNotification(Long id);

	// Get all Notification 
	public List<Notification> getAllNotifications();
		
	// Get Notification By User Id
	public List<Notification> getNotificationByUserId(String userid);
	
	// Get Notification By Topic Id
	public List<Notification> getAllNotificationByTopicId(long topicId);
	
	// Get Notification by UserId and TopicId
	public Notification getNotificationByUserIdAndTopicId(String userId, long topicId);
	
}
