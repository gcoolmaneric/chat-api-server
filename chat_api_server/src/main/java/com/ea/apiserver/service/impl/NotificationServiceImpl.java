package com.ea.apiserver.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ea.apiserver.model.Notification;
import com.ea.apiserver.repository.NotificationRepository;
import com.ea.apiserver.service.NotificationService;

import org.springframework.util.Assert;


@Service
public class NotificationServiceImpl implements NotificationService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private NotificationRepository notifyRepository;

	@Override
	public void addNotification(Notification data) {
		Assert.notNull(data, "Notification must not be null");
		notifyRepository.save(data);
		log.debug("Notification save successfully ");
	}

	@Override
	public Notification updateNotification(Notification data) {
		Assert.notNull(data, "Notification must not be null");
		Notification oldData = notifyRepository.findOne(data.getId());
		Assert.notNull(oldData, "Old User must not be null");
		oldData.setUserId(data.getUserId());
		oldData.setTopicId(data.getTopicId());
		oldData.setStatus(data.getStatus());
		log.debug("{} updated ",oldData);
		return notifyRepository.saveAndFlush(oldData);
	}

	@Override
	public void deleteNotification(Long id) {
		Assert.notNull(id, "Long id must not be null");
		notifyRepository.delete(id);
		log.debug("Notification with Long id {} deleted successfully",  id);
	}

	@Override
	public List<Notification> getAllNotifications() {
		Assert.notNull(notifyRepository.getAllNotifications(), "user list must not be null");
		return notifyRepository.getAllNotifications();
	}
	
	@Override
	public List<Notification> getNotificationByUserId(String userId) {
		return notifyRepository.getAllNotificationByUserId(userId);

	}

	@Override
	public Notification getNotificationByUserIdAndTopicId(String userId, long topicId) 
	{
		return notifyRepository.findByUserIdAndTopicId(userId, topicId);

	}
	
	@Override
	public List<Notification> getAllNotificationByTopicId(long topicId) {
		return notifyRepository.getAllNotificationByTopicId(topicId);

	}
}
