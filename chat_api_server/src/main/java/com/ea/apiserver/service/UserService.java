package com.ea.apiserver.service;

import java.util.List;

import com.ea.apiserver.model.User;

public interface UserService {
	
	// Add User 
	public void addUser(User user);
	
	// Update User 
	public User updateUser(User user);

	// Delete User 
	public void deleteUser(Long userId);

	// Get all Users 
	public List<User> getAllUsers();
	
	// Get Top 10 Users by scores
	public List<User> getUsersByScore(); 
	
	// Get Uses User By User Id
	public User getUserByUserId(String userid);

	// Check User Id does not contain special characters
	public boolean hasSpecialString(String input);
	
}
