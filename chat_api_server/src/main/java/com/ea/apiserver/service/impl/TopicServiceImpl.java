package com.ea.apiserver.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ea.apiserver.model.Topic;
import com.ea.apiserver.repository.TopicRepository;
import com.ea.apiserver.service.TopicService;

import org.springframework.util.Assert;


@Service
public class TopicServiceImpl implements TopicService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TopicRepository topicRepository;

	@Override
	public Topic addTopic(Topic topic) {
		Assert.notNull(topic, "Topic must not be null");
		Topic newTopic = topicRepository.save(topic);
		log.debug("Topic save successfully ");
		return newTopic;
	}

	@Override
	public Topic updateTopic(Topic topic) {
		Assert.notNull(topic, "Topic must not be null");
		Topic oldTopic = topicRepository.findOne(topic.getId());
		Assert.notNull(oldTopic, "Topic User must not be null");
		log.debug("{} updated ",oldTopic);
		return topicRepository.saveAndFlush(oldTopic);
	}

	@Override
	public void deleteTopic(Long topicId) {
		Assert.notNull(topicId, "topicId must not be null");
		topicRepository.delete(topicId);
		log.debug("Topic with topicId {} deleted successfully", topicId);
	}

	@Override
	public Topic getTopicById(long id) 
	{
		Assert.notNull(id, "topic id must not be null");
		return topicRepository.findById(id);
	}
	
	@Override
	public List<Topic> getAllTopics() {
		Assert.notNull(topicRepository.getAllTopics(), "topic list must not be null");
		return topicRepository.getAllTopics();
	}
	
	@Override
	public List<Topic> getAllTopicByTitle(String title) {
		return topicRepository.getAllTopicByTitle(title);

	}
	
	@Override
	public Topic getTopicByTitle(String title) {
		Assert.notNull(title, "title must not be null");
		return topicRepository.getTopicByTitle(title);

	}
	
}
