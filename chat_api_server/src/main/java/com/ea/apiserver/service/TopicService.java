package com.ea.apiserver.service;

import java.util.List;

import com.ea.apiserver.model.Topic;

public interface TopicService {
	
	// Add Topic 
	public Topic addTopic(Topic topic);
	
	// Update Topic 
	public Topic updateTopic(Topic topic);

	// Delete Topic 
	public void deleteTopic(Long topicId);

	// Get all Topics 
	public List<Topic> getAllTopics();
	
	// Get Topic By Id
	public Topic getTopicById(long id);
	
	// Get Top 10 Topics By Title
	public List<Topic> getAllTopicByTitle(String title);
	
	// Get one Topic by Title 
	public Topic getTopicByTitle(String title);
	
	
}
