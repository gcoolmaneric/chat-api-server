package com.ea.apiserver.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ea.apiserver.model.User;
import com.ea.apiserver.repository.UserRepository;
import com.ea.apiserver.service.UserService;

import org.springframework.util.Assert;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public void addUser(User user) {
		Assert.notNull(user, "User must not be null");
		userRepository.save(user);
		log.debug("User save successfully ");
	}

	@Override
	public User updateUser(User user) {
		Assert.notNull(user, "User must not be null");
		User oldUser = userRepository.findOne(user.getId());
		Assert.notNull(oldUser, "Old User must not be null");
		oldUser.setUserId(user.getUserId());
		oldUser.setScore(user.getScore());
		log.debug("{} updated ",oldUser);
		return userRepository.saveAndFlush(oldUser);
	}

	@Override
	public void deleteUser(Long userId) {
		Assert.notNull(userId, "userId must not be null");
		userRepository.delete(userId);
		log.debug("User with userId {} deleted successfully", userId);
	}

	@Override
	public List<User> getAllUsers() {
		Assert.notNull(userRepository.getAllUsers(), "user list must not be null");
		return userRepository.getAllUsers();
	}
	
	@Override
	public User getUserByUserId(String userid) {
		return userRepository.findByUserId(userid);

	}
	
	public boolean hasSpecialString(String input) {

        Pattern pattern = Pattern.compile("[a-zA-Z0-9]*");
        Matcher matcher = pattern.matcher(input);

        if (matcher.matches()) {
            log.info("checkSpecialString string: '"+input + "' doesn't contains special character");
            return false;
        }
        log.info("checkSpecialString string :'"+input + "' contains special character");
        return true;
	}
	
	public List<User> getUsersByScore() {
		Assert.notNull(userRepository.getAllUsersByScore(), "user list must not be null");
		return userRepository.getAllUsersByScore();
	}

}
