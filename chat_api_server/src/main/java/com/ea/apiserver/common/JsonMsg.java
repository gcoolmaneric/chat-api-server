package com.ea.apiserver.common;


public class JsonMsg {

    private int status;
    private String msg;
    private String userId;
    private String topicId;
    
    public JsonMsg(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public JsonMsg(int status, String msg, String userId) {
        this.status = status;
        this.msg = msg;
        this.userId = userId;
    }
    
    public JsonMsg(int status, String msg, String userId, String topicid) {
        this.status = status;
        this.msg = msg;
        this.userId = userId;
        this.topicId = topicid;
    }
    
    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
    
    public String getUserId() {
        return userId;
    }
}
