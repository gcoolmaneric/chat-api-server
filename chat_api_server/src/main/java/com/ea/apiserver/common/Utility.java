package com.ea.apiserver.common;

import java.util.Random;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility  {

	private static final Utility instance = new Utility();
	
	public Utility() {}
	
	public static Utility getInstance() {
		return instance;
	}
	
	public String generateRandomNum() {
		
		final String alphabet = "123456789";
	    final int N = alphabet.length();

        String resp  = "";
        Random r = new Random();
	    for (int i = 0; i < 15; i++) {
	    	resp += alphabet.charAt(r.nextInt(N));
	    }
	    return resp;
	}
	
	public Date getCurrentTime() {
		
		//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		//System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48
		return date;
		
	}
	

}