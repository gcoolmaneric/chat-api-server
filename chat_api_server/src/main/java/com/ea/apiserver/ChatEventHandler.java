package com.ea.apiserver;

import com.ea.apiserver.model.ChatObject;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class ChatEventHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChatEventHandler.class);
    private final SocketIOServer server;

    @Autowired
    public ChatEventHandler(SocketIOServer server) {
        this.server = server;
    }

    @OnEvent(value = "chatevent")
    public void onEvent(SocketIOClient client, AckRequest request, ChatObject data) {
       
        String clientId = client.getSessionId().toString();
    	  LOGGER.debug(String.format("### clientId: %s", clientId));
    	  LOGGER.debug(String.format("### cmd: %s", data.getCmd()));
    	  LOGGER.debug(String.format("### username: %s", data.getUserName()));
    	  LOGGER.debug(String.format("### roomId: %s", data.getRoomId()));
    	  switch(data.getCmd()) 
    	  {
    	  	case 1: // Join a room
    	  		client.joinRoom(data.getRoomId());
    	  	    LOGGER.debug(String.format("clientId: %s joins roomId:  %s  Succ", clientId, data.getRoomId()));
    	  		break;
    	  	case 2: // Leave a room
    	  		client.leaveRoom(data.getRoomId());;
    	  		LOGGER.debug(String.format("clientId: %s leaves roomId:  %s Succ", clientId, data.getRoomId()));
    	  		break;
    	  	case 3: // Broadcast a message to a room. roomId = topic title. 
    	  		ChatObject senddata = new ChatObject(data.getUserName(), "Your topic: "+ data.getRoomId()   +" had a new message : "+ data.getMessage());
    	  		server.getRoomOperations(data.getRoomId()).sendEvent("chatevent", senddata);;
    	  		LOGGER.debug(String.format(" clientId: %s broadcasts a msg to a room Id %s Succ", clientId, data.getRoomId()));
          	  break;
    	  }  	 
    	
    }
     
}