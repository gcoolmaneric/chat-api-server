{
  "config": {
      "target": "http://52.193.222.85:9099",
      "phases": [
        {"duration": 5, "arrivalRate": 3000}
      ]
  },
  "scenarios": [
    {
      "engine": "socketio",
      "flow": [
        {"emit": { "channel": "chatevent", "data": {
            "userName": "1234",
            "message" : "title-1234",
            "roomId" : "EA is cool",
			"cmd" : "1"
            }}
		},
		{"emit": { "channel": "chatevent", "data": {
            "userName": "5678",
            "message" : "title-5678",
            "roomId" : "EA is cool",
			"cmd" : "1"
            }}
		},
		{"emit": { "channel": "chatevent", "data": {
            "userName": "5678",
            "message" : "Hello Chat Server",
            "roomId" : "EA is cool",
			"cmd" : "3"
            }}
		},
		 {"emit": { "channel": "chatevent", "data": {
            "userName": "1234",
            "message" : "title-1234",
            "roomId" : "EA is cool",
			"cmd" : "2"
            }}
		},
		{"emit": { "channel": "chatevent", "data": {
            "userName": "5678",
            "message" : "title-5678",
            "roomId" : "EA is cool",
			"cmd" : "2"
            }}
		}
      ]
    }
  ]
}
------------------------------------------------------------------------



